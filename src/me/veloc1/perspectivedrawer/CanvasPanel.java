/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.veloc1.perspectivedrawer;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author DOM
 */
public class CanvasPanel extends JPanel {

    public static final float LENGTH = 2000f;
    public static final int VIEWPORT_WIDTH = 683;
    public static final int VIEWPORT_HEIGHT = 384;
    private MovingObject mMovingObject;
    private boolean mIsScreening;

    public CanvasPanel() {
        super();
        mMovingObject = MovingObject.HORIZON;
        mMovingObject.setY(300);
        MovingObject.RIGHT.setX(600);
        MovingObject.VIEWPORT.setX(11);
        MovingObject.VIEWPORT.setY(11);
        addMouseListener(new CanvasMouseListener());
        addMouseMotionListener(new CanvasMouseListener());
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D graphics = (Graphics2D) g;

        g.setColor(Color.decode("#eeffee"));
        g.fillRect(0, 0, getWidth(), getHeight());

        g.setColor(Color.decode("#99bb99"));
        graphics.setStroke(new BasicStroke(3));
        if (mMovingObject == MovingObject.HORIZON && ! mIsScreening){
            g.setColor(Color.decode("#99bbbb"));    
        }
        g.drawLine(0, MovingObject.HORIZON.getY(), getWidth(), MovingObject.HORIZON.getY());
        
        g.setColor(Color.decode("#99bb99"));
        if (mMovingObject == MovingObject.VIEWPORT && ! mIsScreening){
            g.setColor(Color.decode("#99bbbb"));    
        }
        g.drawRect(MovingObject.VIEWPORT.getX(), MovingObject.VIEWPORT.getY(), VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        graphics.setStroke(new BasicStroke(1));
        for (int i = MovingObject.VIEWPORT.getX(); i < MovingObject.VIEWPORT.getX() + VIEWPORT_WIDTH; i += 24){
            g.drawLine(i, MovingObject.VIEWPORT.getY(), i, MovingObject.VIEWPORT.getY() + VIEWPORT_HEIGHT);
        }
        
        g.setColor(Color.decode("#aaeeaa"));
        if (mMovingObject == MovingObject.LEFT && ! mIsScreening){
            g.setColor(Color.decode("#99bbbb"));    
        }
        for (int i = 0; i < 360; i += 5f) {
            int x = (int) (Math.sin(i * Math.PI / 180f) * LENGTH) + MovingObject.LEFT.getX();
            int y = (int) (Math.cos(i * Math.PI / 180f) * LENGTH) + MovingObject.HORIZON.getY();
            g.drawLine(MovingObject.LEFT.getX(), MovingObject.HORIZON.getY(), x, y);
        }
        g.setColor(Color.decode("#aaeeaa"));
        if (mMovingObject == MovingObject.RIGHT && ! mIsScreening){
            g.setColor(Color.decode("#99bbbb"));    
        }
        for (int i = 0; i < 360; i += 5f) {
            int x = (int) (Math.sin(i * Math.PI / 180f) * LENGTH) + MovingObject.RIGHT.getX();
            int y = (int) (Math.cos(i * Math.PI / 180f) * LENGTH) + MovingObject.HORIZON.getY();
            g.drawLine(MovingObject.RIGHT.getX(), MovingObject.HORIZON.getY(), x, y);
        }
    }

    void setMoving(MovingObject movingObject) {
        mMovingObject = movingObject;
        repaint();
    }

    void takeScreen() {
        mIsScreening = true;
        Dimension size = this.getSize();
        BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = image.createGraphics();
        this.paint(g2);
        image = image.getSubimage(MovingObject.VIEWPORT.getX() - 10, MovingObject.VIEWPORT.getY() - 10, VIEWPORT_WIDTH + 20, VIEWPORT_HEIGHT + 20);
        try {
            File dir = new File("images");
            if (!dir.exists()){
                dir.mkdir();
            }
            ImageIO.write(image, "png", new File("images/persp" + System.currentTimeMillis() + ".png"));
        } catch (IOException ex) {
            Logger.getLogger(CanvasPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        mIsScreening = false;
    }

    private class CanvasMouseListener implements MouseListener, MouseMotionListener {

        public CanvasMouseListener() {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            switch (mMovingObject) {
                case HORIZON:
                    MovingObject.HORIZON.setY(e.getY());
                    break;
                case VIEWPORT:
                    MovingObject.VIEWPORT.setX(e.getX());
                    MovingObject.VIEWPORT.setY(e.getY());
                    if (MovingObject.VIEWPORT.getX() < 11){
                        MovingObject.VIEWPORT.setX(11);
                    }
                    if (MovingObject.VIEWPORT.getY() < 11){
                        MovingObject.VIEWPORT.setY(11);
                    }
                    
                    // TODO check another border
                    break;
                case LEFT:
                    MovingObject.LEFT.setX(e.getX());
                    break;
                case RIGHT:
                    MovingObject.RIGHT.setX(e.getX());
                    break;
            }
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
        }
    }

    public static enum MovingObject {

        HORIZON, LEFT, RIGHT, TOP, BOTTOM, VIEWPORT;
        private int mX;
        private int mY;

        public int getX() {
            return mX;
        }

        public int getY() {
            return mY;
        }

        public void setX(int x) {
            mX = x;
        }

        public void setY(int y) {
            mY = y;
        }
    }
}
